# Methods for inspecting the OS process' environment.
#
# This module provides methods for getting environment variables, the home
# directory, changing the working directory, etc.
#
# # Modifying variables
#
# Modifying the current process' environment variables isn't supported, as this
# can introduce race conditions, or even unsound/undefined behaviour depending
# on the underlying platform.
#
# Fortunately, the use-case for changing variables is rare and probably better
# served by using a sub process.
#
# For more information:
#
# - https://github.com/chronotope/chrono/issues/499
# - https://github.com/rust-lang/rust/pull/24741
# - https://github.com/rust-lang/rust/issues/27970
# - https://github.com/rustsec/advisory-db/issues/926
import std::fs::path::Path
import std::io::Error

# Returns the value of an environment variable.
#
# # Examples
#
# Obtaining the value of an environment variable:
#
#     import std::env
#
#     env.get('HOME') # => '/home/alice'
fn pub get(name: String) -> Option[String] {
  let val = _INKO.env_get(name)

  if _INKO.is_undefined(val) { Option.None } else { Option.Some(val) }
}

# Returns all defined environment variables and their values.
#
# # Examples
#
# Obtaining all environment variables and their values:
#
#     import std::env
#
#     env.variables # => Map { 'HOME': '/home/alice', ... }
fn pub variables -> Map[String, String] {
  _INKO.env_variables.into_iter.reduce(Map.new) fn (map, name) {
    match get(name) {
      case Some(v) -> {
        map[name] = v
        map
      }
      case None -> map
    }
  }
}

# Returns the path to the current user's home directory.
#
# # Examples
#
# Obtaining the home directory of a user:
#
#     import std::env
#
#     env.home_directory # => '/home/alice'
fn pub home_directory -> Option[Path] {
  let dir = _INKO.env_home_directory

  if _INKO.is_undefined(dir) { Option.None } else { Option.Some(dir.into_path) }
}

# Returns the path to the temporary directory.
#
# # Examples
#
# Obtaining the temporary directory:
#
#     import std::env
#
#     env.temporary_directory # => '/tmp'
fn pub temporary_directory -> Path {
  _INKO.env_temp_directory.into_path
}

# Returns the current working directory.
#
# This method will throw if the directory could not be obtained. Possible
# causes for this could be:
#
# 1. The directory no longer exists.
# 1. You do not have the permissions to access the directory.
#
# # Examples
#
# Obtaining the current working directory:
#
#     import std::env
#
#     try! env.working_directory # => '/home/alice/example'
fn pub working_directory !! Error -> Path {
  let path =
    try _INKO.env_get_working_directory else (e) throw Error.from_int(e)

  path.into_path
}

# Changes the current working directory to the given directory.
#
# This method throws if the directory couldn't be changed.
#
# # Examples
#
# Changing the current working directory:
#
#     import std::env
#
#     try! env.working_directory = '..'
fn pub working_directory=(directory: String) !! Error {
  try {
    _INKO.env_set_working_directory(directory)
  } else (e) {
    throw Error.from_int(e)
  }
}

# Returns an `Array` containing all the commandline arguments passed to the
# current program.
#
# # Examples
#
#     import std::env
#
#     # Assuming this program is executed using `inko foo.inko first second`:
#     env.arguments # => ['first', 'second']
fn pub arguments -> Array[String] {
  _INKO.env_arguments
}

# Returns the path to the current executable.
#
# If the program is executed through a symbolic link, the returned path may
# point to the symbolic link instead of the executable the link points to.
fn pub executable !! Error -> Path {
  let path = try _INKO.env_executable else (e) throw Error.from_int(e)

  path.into_path
}
