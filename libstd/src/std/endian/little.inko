# Methods for converting values to/from little-endian encoded bytes.

# Writes a value interpreted as a 32-bits unsigned integer into `into` as a
# series of bytes, starting at the index `at`.
#
# If `value` is greater than the maximum value of a 32-bits unsigned integer,
# the additional bits are ignored (i.e. the value wraps around).
#
# # Examples
#
#     import std::endian::little
#
#     let bytes = ByteArray.filled(with: 0, times: 4)
#
#     little.write_u32(123456789, into: bytes, at: 0)
#     bytes # => ByteArray.from_array([21, 205, 91, 7])
fn pub write_u32(value: Int, into: mut ByteArray, at: Int) {
  into[at] = value
  into[at + 1] = value >> 8
  into[at + 2] = value >> 16
  into[at + 3] = value >> 24
}

# Writes a value interpreted as a 64-bits signed integer into `into` as a series
# of bytes, starting at the index `at`.
#
# # Examples
#
#     import std::endian::little
#
#     let bytes = ByteArray.filled(with: 0, times: 8)
#
#     little.write_i64(123456789, into: bytes, at: 0)
#     bytes # => ByteArray.from_array([21, 205, 91, 7])
fn pub write_i64(value: Int, into: mut ByteArray, at: Int) {
  into[at] = value
  into[at + 1] = value >> 8
  into[at + 2] = value >> 16
  into[at + 3] = value >> 24
  into[at + 4] = value >> 32
  into[at + 5] = value >> 40
  into[at + 6] = value >> 48
  into[at + 7] = value >> 56
}

# Reads four bytes starting at `at` as a 32-bits signed integer.
#
# # Panics
#
# This method panics if there are less than four bytes available starting at
# `at`.
#
# # Examples
#
#     import std::endian::little
#
#     let bytes = ByteArray.filled(with: 0, times: 4)
#
#     little.write_u32(123456789, into: bytes, at: 0)
#     little.read_u32(from: bytes, at: 0) # => 123456789
fn pub read_u32(from: ref ByteArray, at: Int) -> Int {
  (from[at + 3] << 24) | (from[at + 2] << 16) | (from[at + 1] << 8) | from[at]
}

# Reads eight bytes starting at `at` as a 64-bits signed integer.
#
# # Panics
#
# This method panics if there are less than eight bytes available starting at
# `at`.
#
# # Examples
#
#     import std::endian::little
#
#     let bytes = ByteArray.filled(with: 0, times: 8)
#
#     little.write_i64(123456789, into: bytes, at: 0)
#     little.read_i64(from: bytes, at: 0) # => 123456789
fn pub read_i64(from: ref ByteArray, at: Int) -> Int {
  (from[at + 7] << 56)
    | (from[at + 6] << 48)
    | (from[at + 5] << 40)
    | (from[at + 4] << 32)
    | (from[at + 3] << 24)
    | (from[at + 2] << 16)
    | (from[at + 1] << 8)
    | from[at]
}
