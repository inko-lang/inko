import std::process
import std::test::Tests
import std::time::(Duration, Instant)

class async Proc {
  fn async send_back(value: Int) -> Int {
    value
  }

  fn async error(value: Int) !! Int {
    throw value
  }

  fn async slow {
    process.sleep(Duration.from_secs(5))
  }
}

fn pub tests(t: mut Tests) {
  t.test('process.sleep') fn (t) {
    let start = Instant.new

    process.sleep(Duration.from_millis(10))
    t.true(start.elapsed.to_millis >= 10)
  }

  t.test('process.poll') fn (t) {
    let pending = [async Proc {}.send_back(42), async Proc {}.send_back(42)]
    let mut ready = []

    while pending.length > 0 {
      process.poll(pending).into_iter.each fn (fut) { ready.push(fut.await) }
    }

    t.equal(pending.length, 0)
    t.equal(ready, [42, 42])
  }

  t.test('Future.await') fn (t) {
    let proc = Proc {}

    t.equal(async { proc.send_back(1) }.await, 1)
    t.throw fn move { try { async { proc.error(2) }.await } }
  }

  t.test('Future.await_for') fn (t) {
    let proc = Proc {}
    let fast = async proc.send_back(42)
    let slow = async proc.slow

    t.equal(fast.await_for(Duration.from_secs(5)), Option.Some(42))
    t.true(slow.await_for(Duration.from_millis(10)).none?)
  }
}
