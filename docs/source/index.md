# Introduction

Welcome to the Inko manual. The manual contains a wide range of topics, such as
guides for beginners, the internals of the virtual machine, how to contribute to
Inko, and more.

The manual assumes you have prior experience working with programming languages,
and that you are familiar with terminal emulators.

If you are new to Inko, start by reading through the chapters in the "Getting
started" section.

You can contribute to the manual by submitting changes or bug reports to the
[Inko repository](https://gitlab.com/inko-lang/inko).
